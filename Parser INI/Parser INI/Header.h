#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <fstream>
class Ini {
public:
	struct Param
	{
		std::wstring Name; //название
		std::wstring Value; // значение
	};
	struct Section {
		std::wstring Name; // название секции
		int param_num; // количество параметров
		std::vector <Param> Content; // список параметров
	};
private:
	Param param;
	Section sect;
	std::vector<Section> vector_file;
public:
	Param get_param() {return param;}
	void set_param(Param set_param) { param = set_param; }
	Section get_section() { return sect;}
	void set_section(Section set_section) { sect = set_section; }
	Ini(const wchar_t* filename);
	bool GetSection(std::wstring name, Section& sect);
	bool GetParameter(std::wstring name, std::wstring pname, Param& param);
	~Ini(void);
};
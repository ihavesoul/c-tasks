// ParserINI.cpp: определяет точку входа для консольного приложения.
//
#include "stdafx.h"
#include "Header.h"

using namespace std;
Ini::Ini(const wchar_t* filename)
{
	
	wifstream f;
	wstring tmp;

	Param param;
	Section sect;

	int sec_num = 0; // номер секции
	int par_num = 0; // номер параметра
	int i = 0;
	f.open(filename);
	if (f)
	{
		while (getline(f, tmp).eof())
		{
			// если встречается # или ; , то продолжить. 
			if (tmp[0] == L'#' || tmp[0] == L';')
				continue;
			// если встретилась [ , то это секция 
			if (tmp[0] == L'[')
			{
				if (sec_num)
					vector_file.push_back(sect); // Есть секции или нет, если это первая то ничего не добавлять. Иначе, добавить.
				sect.Content.clear();

				par_num = 0;
				sec_num++;

				i = tmp.find(L'[');
				sect.Name = tmp.substr(1, i - 1);
			}
			else
			{
				i = tmp.find(L'=');
				if (i)
				{
					param.Name = tmp.substr(0, i); // вернуть первый символ
					param.Value = tmp.substr(i + 1, tmp.length()); // вернуть i+1 символ

					sect.Content.push_back(param);
					sect.param_num = ++par_num;

				}
			}
		}
		vector_file.push_back(sect);
		f.close();
	}
}

bool Ini::GetParameter(wstring sname, wstring pname, Param& param)
{
	int Ssize = vector_file.size(); // размер секции
	int Psize = 0; // размер параметра

	for (int i = 0; i<Ssize; i++)
	{
		if (vector_file[i].Name == sname) // Если имя секции  ,совпадает с sname 
		{
			Psize = vector_file[i].Content.size(); // Задаем величину параметра
			for (int j = 0; j<Psize; j++) {
				if (vector_file[i].Content[j].Name == pname)
				{
					param.Name = vector_file[i].Content[j].Name;
					param.Value = vector_file[i].Content[j].Value;
					cout << "You do it,lol";
					return true;
				}
			}
		}
	}
	return false;
}

bool Ini::GetSection(wstring name, Section& sect)
{
	int Ssize = vector_file.size(); //размер секции
	int Psize = 0; //размер параметра

	for (int i = 0; i<Ssize; i++)
	{
		
		if (vector_file[i].Name == name) {
			sect.Content.clear();

			sect.Name = vector_file[i].Name;
			sect.param_num = vector_file[i].param_num;

			Psize = vector_file[i].Content.size();

			sect.Content = vector_file[i].Content;
			
		}
	}
	
	return false; 
}

Ini::~Ini(void) {
	vector_file.clear();
}

int main() {
	Ini parser(L"H:\\in.ini");
	Ini::Param parametr;
	Ini::Section section;
	cout << parser.GetParameter(L"[section]", L"key1",parametr);
	cout << parser.GetSection(L"[section]",section);
	
	getchar();
}